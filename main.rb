# Запрашиваем библиотеку для создания сервера
require 'sinatra'
# Запрашиваем библиотеку для сериализации и десериализации JSON
require 'json'

# Запрашиваем наш исполнитель задач
require './workers/csv_reader.rb'

# Описываем путь на сервере и действие, которое будет происходить при запросе на этот путь
post '/upload' do
  # Если прислали файл
  if params[:file]
    # Находим его в файловой системе
    tmpfile = params[:file][:tempfile]
    name = params[:file][:filename]

    STDERR.puts "Uploading file, original name #{name.inspect}"

    # И ставим асинхронную задачу исполнителю. Он получит сериализованное сообщение, поэтому сложные объекты (модели, файлы) отправлять нельзя
    CsvReader.perform_async(tmpfile.path)

    # Возвращаем результат
    content_type :json
    status 200
    {response: 'ok'}.to_json
  else
    # Нет файла - нет действия
    content_type :json
    status 401
    {response: 'No file selected'}.to_json
  end
end