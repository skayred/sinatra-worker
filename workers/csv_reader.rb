# Запрашиваем библиотеку для запуска асинхронных исполнителей задач
require 'sidekiq'
# Запрашиваем библиотеку для чтения CSV
require 'csv'

class CsvReader
  # Строка, небоходимая для того, чтобы исполнитель стал исполнителем
  include Sidekiq::Worker

  # Собственно, действие
  def perform(filename)
    # Читаем наш файл построчно и выводим первую ячейку каждой строки
    CSV.foreach(filename, :headers => false) do |row|
      puts row[0]
    end
  end
end